package currency;

import org.junit.Before;
import org.junit.Test;

import junit.framework.Assert;


public class PricingTest
{
    private Pricing p;
    @Before
    public void init(){
        p = new Pricing("rates.csv");
    }
    
    @Test
    public void reading_file_test(){
      String output =  p.readFile();
      System.out.println (output);
    }
    
    @Test
    public void given_correct_currencies_will_return_price() throws Exception{
        String output;
        output = p.findPrice ("ZMK","ZAR");
        System.out.println (output);
        Assert.assertEquals ("0.0014", output);
    }
    
    @Test(expected = Exception.class)
    public void given_incorrect_currencies_will_throw_exception() throws Exception{
        p.findPrice ("hello", "JPY");
    }
    
    @Test
    public void given_currencies_with_no_direct_price_calculate_from_USD() throws Exception{
        String output = p.findPrice ("ARS", "CNY");
        System.out.println (output);
        Assert.assertEquals ("1.6523635499999998", output);
    }
}

