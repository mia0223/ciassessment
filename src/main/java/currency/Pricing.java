package currency;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;

public class Pricing
{
    private String fileName;
    public Pricing(String fileName){
        this.fileName = fileName;
    }
    
    public String readFile(){
        String output = "";
        InputStream stream = Pricing.class.getClassLoader().getResourceAsStream(fileName);
        try
        {
            output = IOUtils.toString(stream, "UTF-8");
        }
        catch (IOException e)
        {
            System.out.println (e.getMessage ());
        }
        return output;
    }
    
    public Map<String, String> generateMap(){
        Map<String, String> map = new HashMap<String, String>();
        String raw = readFile();
        String[] temp = raw.split ("\\r?\\n");
        for(int i=1; i<temp.length; i++){
            String cell = temp[i];
            String[] element = cell.split (",");
            map.put (element[0]+","+element[1], element[2]);
        }
        return map;
    }
    
    public String findPrice(String currency1, String currency2) throws Exception{
        String result = "";
        String key = currency1 + "," + currency2;
        Map<String, String> map = generateMap();
        result = map.get(key);
        
        if(result == null){
            String toUSD = map.get (currency1 + ",USD");
            if(toUSD == null)   throw new Exception();
            double inter = Double.parseDouble (toUSD);
            String toCur2 = map.get ("USD," + currency2);
            if(toCur2 == null)  throw new Exception();
            double inter2 = Double.parseDouble (toCur2);
            result = Double.toString(inter * inter2);
        }
        return result;
    }
}


